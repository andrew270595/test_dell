FROM ubuntu:16.04

# variables
ENV  WORKING_DIR=/test

# prepare
COPY Test_folder.robot $WORKING_DIR/

RUN  apt-get update                        && \
     apt-get --yes upgrade                 && \
     apt-get install --yes python-pip      && \
     apt-get install --yes mc nano         && \
     apt-get clean all

RUN pip install robotframework

# arguments for robotframework
RUN mkdir $WORKING_DIR/output && \
    echo  "--variable folder:/boot"                    >> $WORKING_DIR/robot_args.txt && \
    echo  "--debugfile $WORKING_DIR/output/debug.txt"  >> $WORKING_DIR/robot_args.txt && \
    echo  "--output    $WORKING_DIR/output/result.xml" >> $WORKING_DIR/robot_args.txt && \
    echo  "--log       $WORKING_DIR/output/log.htm"    >> $WORKING_DIR/robot_args.txt && \
    echo  "--report    $WORKING_DIR/output/report.htm" >> $WORKING_DIR/robot_args.txt

# execution
ENTRYPOINT cd $WORKING_DIR && robot --argumentfile robot_args.txt Test_folder.robot